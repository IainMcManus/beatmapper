﻿/*
 * Copyright (c) 2015 Allan Pichardo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *  http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using UnityEngine;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

public class SongData {
	public List<double> BeatTimes = new List<double>();
	public string SongName = "Unknown";
	public float SongLength;
	public int AverageBPM;

	public SongData() {
	}
}

/*
 * Make your class implement the interface AudioProcessor.AudioCallbaks
 */
public class BeatCallbackReceiver : MonoBehaviour, AudioProcessor.AudioCallbacks
{
	private float currentScale = 1.0f;
	private float scaleDecayRate = 0.25f;

	public string songName = "unknown";
	public SongData songData = new SongData();

	private double audioStartTime = -1.0f;

	void Start()
	{
		//Select the instance of AudioProcessor and pass a reference
		//to this object
		AudioProcessor processor = FindObjectOfType<AudioProcessor>();
		processor.addAudioCallback(this);
	}


	void Update()
	{
		// Reduce the scale back down to one over time
		if (currentScale >= 1.0f) {
			currentScale = Mathf.Max(currentScale - scaleDecayRate * Time.deltaTime, 1.0f);
			transform.localScale = Vector3.one * currentScale;
		}
	}

	//this event will be called every time a beat is detected.
	//Change the threshold parameter in the inspector
	//to adjust the sensitivity
	public void onOnbeatDetected()
	{
		songData.BeatTimes.Add(AudioSettings.dspTime - audioStartTime);

		currentScale = 2.0f;
		transform.localScale = Vector3.one * currentScale;
	}

	//This event will be called every frame while music is playing
	public void onSpectrum(float[] spectrum)
	{
		if (audioStartTime < 0) {
			audioStartTime = AudioSettings.dspTime;
		}

		//The spectrum is logarithmically averaged
		//to 12 bands

		for (int i = 0; i < spectrum.Length; ++i)
		{
			Vector3 start = new Vector3(i, 0, 0);
			Vector3 end = new Vector3(i, spectrum[i], 0);
			Debug.DrawLine(start, end);
		}
	}

	public void onAudioFinished() {
		// Store the song length and average bpm
		songData.SongLength = (float)(AudioSettings.dspTime - audioStartTime);
		songData.AverageBPM = Mathf.RoundToInt(songData.BeatTimes.Count / (songData.SongLength / 60.0f));
		songData.SongName = songName;

		// Setup XmlSerializer and associate with the menu list's type
		XmlSerializer serializer = new XmlSerializer(songData.GetType());

		// Setup a stream writer to write to the MenuFileName
		using (System.IO.StreamWriter writer = new System.IO.StreamWriter(songName + ".xml"))
		{
			// Serialise the menu list
			serializer.Serialize(writer, songData);

			// Commit changes to disk
			writer.Flush();
		}

		Debug.Log("Song Information Saved!");
	}
}

